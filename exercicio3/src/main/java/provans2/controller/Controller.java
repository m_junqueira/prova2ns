/**
 * 
 */
package provans2.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import provans2.bean.Bean;
import provans2.service.Service;

/**
 * @author marcelo
 *
 */
@RestController
public class Controller {
	
	Service appService = new Service();

	@RequestMapping(value = "/firstChar", method = RequestMethod.GET, headers = "Accept=application/json")
	public Bean firstChar() {
		return new Bean();
	}

	@RequestMapping(value = "/firstChar/{input}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Bean firstChar(@PathVariable String input) {
		
		
		return appService.firstChar(input);
	}
}
