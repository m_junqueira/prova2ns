/**
 * 
 */
package provans2;

/**
 * @author marcelo
 *
 */
public interface Stream {
	
	public char getNext();
	
	public boolean hasNext();

}
