package provans2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author marcelo
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner stdin_ = new Scanner(System.in);		
		Stream teste = new StreamStr( stdin_.nextLine() );
		stdin_.close();
		
		System.out.println( firstChar(teste) );

	}

	/**
	 * posicao da vogal
	 * @param caractere
	 * @return a posicao da vogal ou -1 se não for vogal
	 */
	public static int ehVogal(char caractere) {
		char[] vogaisM = { 'A', 'E', 'I', 'O', 'U' };
		char[] vogaism = { 'a', 'e', 'i', 'o', 'u' };
		
		for(int c0=0; c0<5; c0++) {
			if(caractere==vogaisM[c0] || caractere==vogaism[c0]) return c0;
		}
		return -1;
	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public static char firstChar(Stream input) {
		
		int[] checks = { -1, -1, -1 };
		int[] qtos = { 0, 0, 0, 0, 0 };
		char[] vogais = { 'a', 'e', 'i', 'o', 'u' };
		List<Integer> empilhavogais = new ArrayList<Integer>();
		
		while( input.hasNext() ) {
			char caractere = input.getNext();
			
			checks[0] = checks[1];
			checks[1] = checks[2];
			int pos = ehVogal(caractere);
			checks[2] = pos;
			
			if(pos!=-1) { 
				qtos[pos]++;
				int indx = empilhavogais.indexOf(pos);
				if(indx!=-1) {
					empilhavogais.remove(indx);
					if(empilhavogais.isEmpty()) {
						int qt = 0;
						for(int i=0; i<5 && qtos[i]>1; i++, qt++);
						if(qt==5) break;
					}
				}
			}
			
			if(checks[0]!=-1 && checks[1]==-1 && checks[2]!=-1) {
				if( qtos[pos]==1 ) {
					empilhavogais.add(pos);
				}				
			}
			
		}
		if(!empilhavogais.isEmpty()) return vogais[ empilhavogais.get(0) ];
		System.err.println("firstChar() _ nao encontrou primeira vogal unica na entrada fornecida!");
		return '\0';
	}

}
