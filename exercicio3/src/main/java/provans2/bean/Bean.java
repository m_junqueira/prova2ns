/**
 * 
 */
package provans2.bean;

/**
 * @author marcelo
 *
 */
public class Bean {
	
	String input;
	String output;
	
	public Bean() {
		super();
		this.input = "empty";
		this.output = "empty";
	}
	
	public Bean(String input_, String output_) {
		super();
		this.input = input_;
		this.output = output_;		
	}

	public Bean(String input_, char output_) {
		super();
		this.input = input_;
		this.output = new StringBuilder().append("").append(output_).toString();;
	}
	
	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

}
