package provans2.service;

import provans2.App;
import provans2.Stream;
import provans2.StreamStr;
import provans2.bean.Bean;

public class Service {
	
	static App app;
	
	public Service() {
		super();
	}
	
	public Bean firstChar(String from) {
		Stream conteudo = new StreamStr(from);
		Bean response = new Bean( from, app.firstChar(conteudo) );
		return response;
	}

}
