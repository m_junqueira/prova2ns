/**
 * 
 */
package provans2;

/**
 * @author marcelo
 *
 */
public class StreamStr implements Stream {
	
	static char[] conteudo;
	static int pos;
	static int size_conteudo;
	static boolean ended;
	
	public StreamStr(String conteudo_) {
		
		if(conteudo_!=null || conteudo_.length()>0) {
			pos = 0;
			ended = false;
			size_conteudo = conteudo_.length();
			conteudo = new char[conteudo_.length()+1];
			System.arraycopy(conteudo_.toCharArray(), 0, conteudo, 0, size_conteudo);
		}
		else ended = true;
	}
	
	public char getNext() {
		if(!ended) {
			char retorno = conteudo[pos];
			if((++pos)==size_conteudo) ended = true;
			return retorno;
		}
		return '\0';
	}
	
	public boolean hasNext() {
		return !ended;
	}

}
