package provans2;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {
	
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Testa se o resultado da função firsctChar() está correta
     */
    public void testApp()
    {
		Stream teste = new StreamStr("aAbBABacafe");
		char caractere = App.firstChar(teste);
    	
        assertTrue( caractere=='e' );
    }

}
