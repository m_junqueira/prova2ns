/**
 * 
 */
package provans2.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import provans2.bean.Campanha;
import provans2.bean.Torcedor;
import provans2.service.CampanhaService;
import provans2.service.TorcedorService;

/**
 * @author marcelo
 *
 */

@RestController
public class AllControllers {
	
	CampanhaService campanhaService = new CampanhaService();
	TorcedorService torcedorService = new TorcedorService();

	
	// Campanhas -- exercicio 1
	@RequestMapping(value = "/Campanhas", method = RequestMethod.GET, headers = "Accept=application/json")
	public List getCampanhas() {
		List campanhas = campanhaService.getCampanhas();
		return campanhas; 
	}
	
	@RequestMapping(value = "/Campanha/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Campanha getCampanha(@PathVariable int id) {
		 return campanhaService.getCampanha(id);
	}
	
	@RequestMapping(value = "/Campanhas", method = RequestMethod.POST, headers = "Accept=application/json")
	public Campanha addCampanha(@RequestBody Campanha campanha) {
		return campanhaService.addCampanha( campanha );
	}

	@RequestMapping(value = "/Campanhas", method = RequestMethod.PUT, headers = "Accept=application/json")
	public Campanha updateCampanha(@RequestBody Campanha campanha) {
		return campanhaService.updateCampanha(campanha);

	}

	@RequestMapping(value = "/Campanha/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public void deleteCampanha(@PathVariable("id") int id) {
		campanhaService.deleteCampanha(id);
	}

	
	// Torcedores -- exercicio 2
	@RequestMapping(value = "/Torcedores", method = RequestMethod.GET, headers = "Accept=application/json")
	public List getTorcedores() {
		return torcedorService.getTorcedores();
	}
	
	@RequestMapping(value = "/Torcedor/{email}", method = RequestMethod.GET, headers = "Accept=application/json")
	public Torcedor getTorcedor(@PathVariable String email) {
		 return torcedorService.getTorcedor(email);
	}
	
	@RequestMapping(value = "/Torcedores", method = RequestMethod.POST, headers = "Accept=application/json")
	public List addTorcedor(@RequestBody Torcedor torcedor) {
		int resul = torcedorService.addTorcedor( torcedor );
		if(resul==0) {
			List<String> resposta = new ArrayList<String>();
			resposta.add("Cadastrado com sucesso!");
			return resposta;		
		} else if(resul==1) return campanhaService.getCampanhasTime( torcedor.getTimeCoracao() );
		
		return null;
	}

	@RequestMapping(value = "/Torcedores", method = RequestMethod.PUT, headers = "Accept=application/json")
	public Torcedor updateTorcedor(@RequestBody Torcedor torcedor) {
		return torcedorService.updateTorcedor(torcedor);

	}

	@RequestMapping(value = "/Torcedor/{email}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public void deleteTorcedor(@PathVariable("email") String email) {
		torcedorService.deleteTorcedor(email);
	}

}
