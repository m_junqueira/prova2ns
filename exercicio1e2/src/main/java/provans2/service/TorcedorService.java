/**
 * 
 */
package provans2.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import provans2.bean.Torcedor;

/**
 * @author marcelo
 *
 */
public class TorcedorService {
	
	static HashMap<String, Torcedor> idTorcedorMap = getIdTorcedorMap();

	/**
	 * 
	 */
	public TorcedorService() {
		super();
	}
	/**
	 * 
	 * @return lista das Torcedors
	 */
	public List getTorcedores() {
		List torcedores = new ArrayList(idTorcedorMap.values());
		//
		return torcedores;
	}

	/**
	 * 
	 * @param id identificador da Torcedor
	 * @return Torcedor 
	 */
	public Torcedor getTorcedor(String email) {
		Torcedor torcedor = idTorcedorMap.get(email);
		return torcedor;
	}
	
	/**
	 * Insere Torcedor na base
	 * @param Torcedor
	 * @return
	 */
	public int addTorcedor(Torcedor torcedor) {
		if(torcedor!=null) {
			Torcedor quem = getTorcedor(torcedor.getEmail());
			if(quem==null) {
				idTorcedorMap.put(torcedor.getEmail(), torcedor);
				return 0;
			}
			return 1;
		}
		return -1;
	}
	
	/**
	 * 
	 * @param Torcedor
	 * @return
	 */
	public Torcedor updateTorcedor(Torcedor torcedor) {
		// if( Torcedor.getIdTorcedor()<=0 ) return null;
		// idTorcedorMap.put(Torcedor.getIdTorcedor(), Torcedor);
		return torcedor;
	}
	
	/**
	 * 
	 * @param id
	 */	
	public void deleteTorcedor(String email) {
		idTorcedorMap.remove(email);
	}

	
	public static HashMap<String, Torcedor> getIdTorcedorMap() {
		return idTorcedorMap;
	}
	
}
