/**
 * 
 */
package provans2.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import provans2.bean.Campanha;

/**
 * @author marcelo
 * 
 * Classe auxiliar utilizando container.
 * Futuramente pode ser substituida por uma implementacao de Banco de Dados
 *
 */
public class CampanhaService {

	// static int idNum = 0;
	static HashMap<Integer, Campanha> idCampanhaMap = getIdCampanhaMap();
	
	/**
	 * 
	 */
	public CampanhaService() {
		super();
		if(idCampanhaMap==null) {
			idCampanhaMap = new HashMap<Integer, Campanha>();
		}
	}
	
	/**
	 * 
	 * @return lista das campanhas
	 */
	public List getCampanhas() {
		if( idCampanhaMap.isEmpty() ) {
			HashMap<Integer, Campanha> idCampanhaMapTmp = new HashMap<Integer, Campanha>();
			idCampanhaMapTmp.put(0, new Campanha());
			List<Campanha> campanhas = new ArrayList<Campanha>(idCampanhaMapTmp.values());
			return campanhas;
		}
		
		List<Campanha> campanhas = new ArrayList<Campanha>(idCampanhaMap.values());
		LocalDate today = LocalDate.now();
		for(int c0=0; c0<campanhas.size(); c0++) {
			while(c0<campanhas.size() && campanhas.get(c0).vencido(today)) campanhas.remove(c0);
		}
		return campanhas;
	}
	
	public List getCampanhasTime(int numTime) {
		List<Campanha> campanhas = new ArrayList<Campanha>(idCampanhaMap.values());
		for(int c0=0; c0<campanhas.size(); c0++) {
			while(c0<campanhas.size() && campanhas.get(c0).getTimeCoracao()!=numTime) campanhas.remove(c0);
		}
		return campanhas;
	}

	/**
	 * 
	 * @param id identificador da campanha
	 * @return campanha 
	 */
	public Campanha getCampanha(int id) {
		Campanha campanha = idCampanhaMap.get(id);
		return campanha;
	}
	
	/**
	 * Insere campanha na base
	 * @param campanha
	 * @return
	 */
	public Campanha addCampanha(Campanha campanha) {
		campanha.setIdCampanha(getMaxId()+1);		
		idCampanhaMap.forEach( (k,v)->v.ajustaPeriodo(campanha.dataInicio, campanha.dataFim) );		
		idCampanhaMap.put( campanha.getIdCampanha(),  campanha);
		return campanha;
	}
	
	/**
	 * 
	 * @param campanha
	 * @return
	 */
	public Campanha updateCampanha(Campanha campanha) {
		if( campanha.getIdCampanha()<=0 ) return null;
		idCampanhaMap.put(campanha.getIdCampanha(), campanha);
		return campanha;
	}
	
	/**
	 * 
	 * @param id
	 */	
	public void deleteCampanha(int id) {
		idCampanhaMap.remove(id);
	}
	
	public static HashMap<Integer, Campanha> getIdCampanhaMap() {
		return idCampanhaMap;
	}
	
	public static int getMaxId() {
		int max=0;
		for (int id:idCampanhaMap.keySet()) { 
			if(max<=id) max=id;
		}
		return max;
	}

}
