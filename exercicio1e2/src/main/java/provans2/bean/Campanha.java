/**
 * 
 */
package provans2.bean;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author marcelo
 *
 */
public class Campanha {
	
	int idCampanha;
	String nomeCampanha;
	int timeCoracao;
	public LocalDate dataInicio;
	public LocalDate dataFim;
	
	

	/**
	 * 
	 */
	public Campanha() {
		super();
	}
	
	public Campanha(int idCampanha, String nomeCampanha, int timeCoracao, LocalDate dataInicio,	LocalDate dataFim) {
		super();
		this.idCampanha = idCampanha;
		this.nomeCampanha = nomeCampanha;
		this.timeCoracao = timeCoracao;
		this.dataFim = dataFim;
		this.dataInicio = dataInicio;
	}

	public boolean vencido(LocalDate atual) {
		if(dataFim!=null) return dataFim.compareTo( atual )<0;
		return true; // se for null não retorna ok
	}
	
	public void ajustaPeriodo(LocalDate dataI, LocalDate dataF) {
		if(dataI==null) dataI = LocalDate.MAX;
		if(dataF==null) dataF = LocalDate.MIN;

		if(dataInicio!=null && dataFim!=null) {
			if( dataF.compareTo(this.dataInicio)>=0 && dataF.compareTo(this.dataFim)<=0 ) this.dataFim = this.dataFim.plusDays(1);
			else
			if( dataI.compareTo(this.dataInicio)>=0 && dataI.compareTo(this.dataFim)<=0 ) this.dataFim = this.dataFim.plusDays(1);
			
			if(this.dataFim.compareTo(dataF)==0) this.dataFim = this.dataFim.plusDays(1);
		}

	}

	public int getIdCampanha() {
		return idCampanha;
	}



	public void setIdCampanha(int idCampanha) {
		this.idCampanha = idCampanha;
	}



	public String getNomeCampanha() {
		return nomeCampanha;
	}



	public void setNomeCampanha(String nomeCampanha) {
		this.nomeCampanha = nomeCampanha;
	}



	public int getTimeCoracao() {
		return timeCoracao;
	}



	public void setTimeCoracao(int timeCoracao) {
		this.timeCoracao = timeCoracao;
	}



	public /* LocalDate */ String getDataInicio() {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		if(dataInicio!=null) return dataInicio.format(dtf);
		return null;
	}
	
	/* public LocalDate getLDInicio() {
		return dataInicio;
	}*/



	public void setDataInicio(String dataInicio) { //LocalDate dataInicio) {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		final LocalDate dt = LocalDate.parse(dataInicio,dtf);			
		this.dataInicio = dt;
	}



	public /* LocalDate */ String getDataFim() {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		if(dataFim!=null) return dataFim.format(dtf);
		return null;
	}

	/* public LocalDate getLDFim() {
		return dataFim;
	} */


	public void setDataFim(String dataFim) { // LocalDate dataFim) {
		final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		final LocalDate dt = LocalDate.parse(dataFim,dtf);			
		this.dataFim = dt;
	}

}
