/**
 * 
 */
package provans2.bean;

import java.time.LocalDate;

/**
 * @author marcelo
 *
 */
public class Torcedor {

	String NomeCompleto;
	String email;
	LocalDate dataNascimento;
	int timeCoracao;
	
	/**
	 * 
	 */
	public Torcedor() {
		super();
	}
	
	public Torcedor(String NomeCompleto, String email, LocalDate dataNascimento, int timeCoracao) {
		super();
		this.NomeCompleto = NomeCompleto;
		this.email = email;
		this.dataNascimento = dataNascimento;
		this.timeCoracao = timeCoracao;
	}

	public String getNomeCompleto() {
		return NomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		NomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public int getTimeCoracao() {
		return timeCoracao;
	}

	public void setTimeCoracao(int timeCoracao) {
		this.timeCoracao = timeCoracao;
	}

}
