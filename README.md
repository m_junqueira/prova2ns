# Exercicios da prova Java 1.1.b

Respostas:

## Exercício 1 e 2:

	Framework Spring
	Maven Build para gerenciar o projeto
	Testado no servidor de aplicação Tomcat 8.04
	
	A aplicação: 
		Exposição dos serviços: Campanha e Torcedor
		Utilizando os serviços do http POST, GET, UPDATE e DELETE como equivalência ao CRUD.
		### GET e POST
		[servidor]/exercicio1e2/Campanhas -> para inserir e listar as campanhas
		[servidor]/exercicio1e2/Torcedores -> para inserir e lista os torcedores
		
		### GET, UPDATE e DELETE
		[servidor]/exercicio1e2/Campanha/{id} -> para obter, atualizar ou apagar o registro de campanha do {id}
		[servidor]/exercicio1e2/Torcedor/{email} -> para obter, atualizar ou apagar o registro de torcedor do {email}


## Exercício 3:

	Função firstChar(Stream input) da classe App ( public App.firstChar(Stream) ) retorna a vogal conforme as regras
	do exercício.
	
	A aplicação:
		Como não foi definido como seria a entrada de dados montei 3 cenários:
		1) Projeto Spring/Rest onde expus o comando GET para receber a string e retornar o resultado.
		   Para isto é necessário um servidor de aplicação.
		2) Também pode ser utilizado como aplicação SE (standalone) onde recebe a entrada de dados pelo stdin.
		3) JUnit para testar os resultados.
		

## Exercício 4:

	Deadlock é uma situação que ocorre quando 2 ou mais threads travam ou ficam bloqueadas eternamente, uma esperando
	que a outra libere o processo para continuar.
	
	É mais comum ocorrer esta situação quando há mais de um ponto de sincronismo nas threads e são utilizados para estes
	mais de um objeto de sincronia. Para evitar deve-se ordenar o uso dos objetos de sincronismos de forma que sejam 
	chamados em situações equivalentes.
	
	Por exemplo, se temos os objetos de sincronismo LOCK1 e LOCK2 e as threads THREAD1 e THREAD2, ao executarmos ambas as
	threads:
	
		_Situação de Deadlock_
		
		THREAD1 {
			syncronized(LOCK1) {
				...
				syncronized(LOCK2) { ... }
			}
		
		THREAD2 {
			syncronized(LOCK2) {
				...
				syncronized(LOCK1) { ... }
			}
			
		_Situação reorganizado para evitar Deadlock_

		THREAD1 {
			syncronized(LOCK1) {
				...
				syncronized(LOCK2) { ... }
			}
		
		THREAD2 {
			syncronized(LOCK1) {
				...
				syncronized(LOCK2) { ... }
			}

## Exercício 5:

	A API Stream do Java 8 contém classes para processar sequencia de elementos ou coleções de objetos. Para que este 
	processamento possa ser executado paralelamente em múltiplas threads foi disponibilizado o método parallelStream()
	
	Lembrando-se que sempre há algum overhead no uso de threads, então nem sempre há ganhos no processamento paralelo.
	Inclusive, os ganhos só são visíveis em máquinas com multiplos core, em que se aproveita melhor as threads rodando em
	cada core do sistema. Mas, além disso, há momentos que é melhor optar por ṕrocessamentos simples. Por exemplo, quando
	o processamento de um elemento depende exclusivamente do processamento do elemento anterior ou em sequencia é melhor
	usar o Stream. Se for possível massificar o resultado, não importa em que ordem os elementos são processados, melhor 
	usar o parallelStream, como numa soma de umm atributo, por exemplo.
	
	


